import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Message } from '../../../../common/communication/message';
import { Planrepas } from '../../../../common/tables/Planrepas';
import { PopUpComponent } from '../pop-up/pop-up.component';
import { CommunicationService } from '../services/communication.service';

@Component({
  selector: 'app-delete-planrepas',
  templateUrl: './delete-planrepas.component.html',
  styleUrls: ['./delete-planrepas.component.css']
})
export class DeletePlanrepasComponent implements OnInit {

  public planrepasList: Planrepas[] = [];

  constructor(public dialog: MatDialog, private communicationService: CommunicationService) { }

  ngOnInit(): void {
    this.getPlanrepas();
  }

  public deletePlanrepas(index: number): void {
    this.communicationService.delete(this.planrepasList[index].numeroplan).subscribe((res: any) => {
      this.getPlanrepas();
    });
    const message = {
      title: "Succes",
      body: "Supprimé avec succès"
    };
    this.openDialog(message);
  }
  
  public getPlanrepas(): void {
    this.communicationService.getPlanrepas().subscribe((planrepas: Planrepas[]) => {
      this.planrepasList = planrepas;
      console.log("in:");
      console.table(this.planrepasList);
    });
    console.log("out:");
    console.table(this.planrepasList);
  }

  openDialog(message: Message) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '650px';
    dialogConfig.data = {
      title: message.title,
      body: message.body,
    }
    this.dialog.open(PopUpComponent, dialogConfig);
  }

}
