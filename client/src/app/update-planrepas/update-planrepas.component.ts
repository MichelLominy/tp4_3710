import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Message } from '../../../../common/communication/message';
import { Fournisseur } from '../../../../common/tables/Fournisseur';
import { Planrepas } from '../../../../common/tables/Planrepas';
import { PopUpComponent } from '../pop-up/pop-up.component';
import { CommunicationService } from '../services/communication.service';
import { ValidationService } from '../services/validation.service';

@Component({
  selector: 'app-update-planrepas',
  templateUrl: './update-planrepas.component.html',
  styleUrls: ['./update-planrepas.component.css']
})
export class UpdatePlanrepasComponent implements OnInit {
  
  public planrepasList: Planrepas[] = [];
  public fournisseurList : Fournisseur[] = [];
  public duplicateError: boolean = false;

  public constructor(public dialog: MatDialog, private communicationService: CommunicationService, private validationService: ValidationService) { }

  ngOnInit(): void {
    this.getPlanrepas();
    this.getFournisseur();
  }
  public getPlanrepas(): void {
    this.communicationService.getPlanrepas().subscribe((planrepas: Planrepas[]) => {
      this.planrepasList = planrepas;
    });
  }

  public getFournisseur(): void {
    this.communicationService.getFournisseur().subscribe((fournisseur: Fournisseur[]) => {
      this.fournisseurList = fournisseur;
    });
  }
  
  public changeNumeroFournisseur(event: any, i:number){
    console.log(event.value);
    this.planrepasList[i].numerofournisseur = event.value;
  }

  public changeCategorie(event: any, i:number){
    const editField = event.target.textContent;
    this.planrepasList[i].categorie = editField;
  }

  public changeFrequence(event: any, i:number){
    const editField = event.target.textContent;
    this.planrepasList[i].frequence = editField;
  }

  public changeNbrPersonne(event: any, i:number){
    const editField = event.target.textContent;
    this.planrepasList[i].nbrpersonnes = editField;
  }

  public changeCalorie(event: any, i:number){
    const editField = event.target.textContent;
    this.planrepasList[i].nbcalories = editField;
  }

  public changePrix(event: any, i:number){
    const editField = event.target.textContent;
    this.planrepasList[i].prix = editField;
  }

  public updatePlanrepas(i: number) {
    const message = this.validationService.isUpdatedPlanrepasValid(this.planrepasList[i]);
    if(message.title === "Succès")
      this.communicationService.updatePlanrepas(this.planrepasList[i]).subscribe((res: any) => {
        this.getPlanrepas();
      });
    else
      this.getPlanrepas();
    this.openDialog(message);
  }

  openDialog(message: Message) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '650px';
    dialogConfig.data = {
      title: message.title,
      body: message.body,
    }
    this.dialog.open(PopUpComponent, dialogConfig);
  }

}