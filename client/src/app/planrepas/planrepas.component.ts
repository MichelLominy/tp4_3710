import { Component, OnInit } from '@angular/core';
import { Planrepas } from '../../../../common/tables/Planrepas';
import { CommunicationService } from '../services/communication.service';

@Component({
  selector: 'app-planrepas',
  templateUrl: './planrepas.component.html',
  styleUrls: ['./planrepas.component.css']
})
export class PlanrepasComponent implements OnInit {
  
  public planrepasList: Planrepas[] = [];

  public constructor(private communicationService: CommunicationService) { }

  ngOnInit(): void {
    this.getPlanrepas();
  }
  public getPlanrepas(): void {
    this.communicationService.getPlanrepas().subscribe((planrepas: Planrepas[]) => {
      this.planrepasList = planrepas;
    });
  }
}