import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AddPlanrepasComponent } from "../add-planrepas/add-planrepas.component";

import { AppComponent } from "../app.component";
import { DeletePlanrepasComponent } from "../delete-planrepas/delete-planrepas.component";
import { PlanrepasComponent } from "../planrepas/planrepas.component";
import { UpdatePlanrepasComponent } from "../update-planrepas/update-planrepas.component";

const routes: Routes = [
  { path: "app", component: AppComponent },
  { path: "Planrepas", component: PlanrepasComponent},
  { path: "Planrepas/Insert", component: AddPlanrepasComponent},
  { path: "Planrepas/Update", component: UpdatePlanrepasComponent},
  { path: "Planrepas/Delete", component: DeletePlanrepasComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
