import { Injectable } from '@angular/core';
import { Message } from '../../../../common/communication/message';
import { Planrepas } from '../../../../common/tables/Planrepas';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

    isAddedPlanrepasValid(planRepas : Planrepas, planrepasList: Planrepas[]): Message{
        const message: Message = {
            title:"Succès",
            body: ""
        }
        
        if(!this.isNumeroPlanValid(planRepas.numeroplan)){
            message.body =  message.body.concat(`NumeroPlan(${planRepas.numeroplan}) n'est pas un nom de numeroplan valide, veuillez saisir un numero de plan entre 0 et 4 caractères respectant le format: (p + Nombre). `) + "\n";
        }else if(this.isNumeroPlanAlreadyExist(planRepas.numeroplan, planrepasList)){
            message.body =  message.body.concat(`NumeroPlan(${planRepas.numeroplan}) est déjà présent dans la liste des Planrepas. `) + "\n";
        }

        if(!this.isCategorieValid(planRepas.categorie) || !planRepas.categorie){
            message.body = message.body.concat(`Catégorie(${planRepas.categorie}) n'est pas un nom de catégorie valide, veuillez saisir une catégorie entre 0 et 15 charactères. `) + "\n";
        }

        if(!this.isNumber(planRepas.frequence) || !planRepas.frequence){
            message.body = message.body.concat(`Fréquence(${planRepas.frequence}) n'est pas un entier, veuillez saisir une valeur entière pour le champ. `) + "\n";
        }

        if(!this.isNumber(planRepas.nbrpersonnes) || !planRepas.nbrpersonnes){
            message.body = message.body.concat(`Nombre de personne(${planRepas.nbrpersonnes}) n'est pas un entier, veuillez saisir une valeur entière pour le champ. `) + "\n";
        }

        if(!this.isNumber(planRepas.nbcalories) || !planRepas.nbcalories){
            message.body = message.body.concat(`Nombre de calorie(${planRepas.nbcalories}) n'est pas un entier, veuillez saisir une valeur entière pour le champ. `) + "\n";
        }

        if(!this.isNumber(planRepas.prix) || !planRepas.prix){
            message.body = message.body.concat(`Prix(${planRepas.prix}) n'est pas un entier, veuillez saisir une valeur entière pour le champ. `) + "\n";
        }
        message.title = message.body.length === 0 ? "Succès" : "Erreur";
        message.body = message.title === "Succès" ? "L'ajout s'est bien éffectué. " : message.body;

        return message;
    }

    isUpdatedPlanrepasValid(planRepas : Planrepas):Message{
        const message: Message = {
            title:"Succès",
            body: ""
        }
        
        if(!this.isCategorieValid(planRepas.categorie)){
            message.body = message.body.concat(`Catégorie(${planRepas.categorie}) n'est pas un nom de catégorie valide, veuillez saisir une catégorie entre 0 et 15 charactères. `) + "\n";
        }

        if(!this.isNumber(planRepas.frequence) || !planRepas.frequence){
            message.body = message.body.concat(`Fréquence(${planRepas.frequence}) n'est pas un entier, veuillez saisir une valeur entière pour le champ. `) + "\n";
        }

        if(!this.isNumber(planRepas.nbrpersonnes) || !planRepas.nbrpersonnes){
            message.body = message.body.concat(`Nombre de personne(${planRepas.nbrpersonnes}) n'est pas un entier, veuillez saisir une valeur entière pour le champ. `) + "\n";
        }

        if(!this.isNumber(planRepas.nbcalories) || !planRepas.nbcalories){
            message.body = message.body.concat(`Nombre de calorie(${planRepas.nbcalories}) n'est pas un entier, veuillez saisir une valeur entière pour le champ. `) + "\n";
        }

        if(!this.isNumber(planRepas.prix) || !planRepas.prix){
            message.body = message.body.concat(`Prix(${planRepas.prix}) n'est pas un entier, veuillez saisir une valeur entière pour le champ. `) + "\n";
        }

        message.title = message.body.length === 0 ? "Succès" : "Erreur";
        message.body = message.title === "Succès" ? "La mise à jour s'est bien éffectué." : message.body;

        return message;
    }


  private isNumeroPlanAlreadyExist(numeroPlan: string, planrepasList: Planrepas[]) : boolean {
    let exist = false;
    planrepasList.forEach((planrepas: Planrepas) =>{
        if(numeroPlan === planrepas.numeroplan && !exist)
            exist = true;
    });
    return exist
  }

  private isNumeroPlanValid(numeroplan : any) : boolean{
    return numeroplan && numeroplan.length  > 0 && numeroplan.length <= 4 && typeof numeroplan === 'string' && numeroplan[0] === 'p' && this.isNumber(numeroplan.substring(1));
  }

  private isCategorieValid(categorie : any) : boolean{
    return categorie && categorie.length  > 0 && categorie.length <= 15;
  }

  private isNumber(textField : any) : boolean{
    return !isNaN(Number(textField));
  }
  
}
