// À DÉCOMMENTER ET À UTILISER LORSQUE VOTRE COMMUNICATION EST IMPLÉMENTÉE
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, Observable, of, Subject } from "rxjs";
import {Planrepas} from  "../../../../common/tables/Planrepas"
import {Fournisseur} from "../../../../common/tables/Fournisseur"
@Injectable()
export class CommunicationService {
  // À DÉCOMMENTER ET À UTILISER LORSQUE VOTRE COMMUNICATION EST IMPLÉMENTÉE
  private readonly BASE_URL: string = "http://localhost:3000/database";
  public constructor(private readonly http: HttpClient) {}

  private _listeners: any = new Subject<any>();

  listen(): Observable<any> {
    return this._listeners.asObservable();
  }

  filter(filterBy: string): void {
    this._listeners.next(filterBy);
  }

  public getPlanrepas(): Observable<Planrepas[]> {
    return this.http
      .get<Planrepas[]>(this.BASE_URL + "/Planrepas")
      .pipe(catchError(this.handleError<Planrepas[]>("getPlanrepas")));
  }
  
  public getFournisseur(): Observable<Fournisseur[]> {
    return this.http
      .get<Fournisseur[]>(this.BASE_URL + "/Fournisseur")
      .pipe(catchError(this.handleError<Fournisseur[]>("getFournisseur")));
  }

  public insertPlanrepas(planrepas: Planrepas) {
    return this.http
      .post<number>(this.BASE_URL + "/Planrepas/insert", planrepas)
      .pipe(catchError(this.handleError<number>("insertPlanrepas")));
  }

  public updatePlanrepas(planrepas: Planrepas) {
    return this.http
      .put<number>(this.BASE_URL + "/Planrepas/update", planrepas)
      .pipe(catchError(this.handleError<number>("updatePlanrepas")));
  }

  public delete(numeroplan: string){ 
    return this.http
    .delete<number>(this.BASE_URL + "/Planrepas/" + numeroplan)
    .pipe(catchError(this.handleError<number>("deletePlanrepas")));
  }

  // À DÉCOMMENTER ET À UTILISER LORSQUE VOTRE COMMUNICATION EST IMPLÉMENTÉE
  private handleError<T>(
    request: string,
    result?: T
  ): (error: Error) => Observable<T> {
    return (error: Error): Observable<T> => {
      return of(result as T);
    };
  }
}
