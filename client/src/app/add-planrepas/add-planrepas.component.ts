import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Planrepas } from '../../../../common/tables/Planrepas';
import { CommunicationService } from '../services/communication.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PopUpComponent } from '../pop-up/pop-up.component';
import { Fournisseur } from '../../../../common/tables/Fournisseur';
import {Message} from '../../../../common/communication/message'
import { ValidationService } from '../services/validation.service';
@Component({
  selector: 'app-add-planrepas',
  templateUrl: './add-planrepas.component.html',
  styleUrls: ['./add-planrepas.component.css']
})
export class AddPlanrepasComponent implements OnInit {
  @ViewChild("newNumeroPlanrepas") newNumeroPlanrepas: ElementRef;
  @ViewChild("newCategorie") newCategorie: ElementRef;
  @ViewChild("newFrequence") newFrequence: ElementRef;
  @ViewChild("newNombrePersonne") newNombrePersonne: ElementRef;
  @ViewChild("newNombreCalorie") newNombreCalorie: ElementRef;
  @ViewChild("newPrix") newPrix: ElementRef;
  newNumeroFournisseur : string;
  
  public planrepasList: Planrepas[] = [];
  public fournisseurList : Fournisseur[] = [];

  public constructor(public dialog: MatDialog, private communicationService: CommunicationService, private validationService: ValidationService) { }

  ngOnInit(): void {
    this.getPlanrepas();
    this.getFournisseur();
  }


  openDialog(message: Message) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = '650px';
    dialogConfig.data = {
      title: message.title,
      body: message.body,
    }
    this.dialog.open(PopUpComponent, dialogConfig);
  }

  public getFournisseur(): void {
    this.communicationService.getFournisseur().subscribe((fournisseur: Fournisseur[]) => {
      this.fournisseurList = fournisseur;
    });
  }

  public getPlanrepas(): void {
    this.communicationService.getPlanrepas().subscribe((planrepas: Planrepas[]) => {
      this.planrepasList = planrepas;
    });
  }

  public insertPlanrepas(): void {
    const planrepas: any = {
      numeroplan: this.newNumeroPlanrepas.nativeElement.innerText,
      numerofournisseur: this.newNumeroFournisseur,
      categorie: this.newCategorie.nativeElement.innerText,
      frequence: this.newFrequence.nativeElement.innerText,
      nbrpersonnes: this.newNombrePersonne.nativeElement.innerText,
      nbcalories: this.newNombreCalorie.nativeElement.innerText,
      prix: this.newPrix.nativeElement.innerText,
    };

    const message = this.validationService.isAddedPlanrepasValid(planrepas, this.planrepasList);
    if(message.title === "Succès")
      this.communicationService.insertPlanrepas(planrepas).subscribe((res: number) => {
        if (res > 0) {
          this.communicationService.filter("update");
        }
        this.refresh();
      });
    this.openDialog(message);
  }

  public refresh(): void {
    this.getPlanrepas();
    this.newNumeroPlanrepas.nativeElement.innerText = "";
    this.newNumeroFournisseur = "";
    this.newCategorie.nativeElement.innerText = "";
    this.newFrequence.nativeElement.innerText = "";
    this.newNombrePersonne.nativeElement.innerText = "";
    this.newNombreCalorie.nativeElement.innerText = "";
    this.newPrix.nativeElement.innerText = "";
  }
}