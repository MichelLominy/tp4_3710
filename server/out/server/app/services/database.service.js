"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseService = void 0;
const inversify_1 = require("inversify");
const pg = require("pg");
require("reflect-metadata");
let DatabaseService = class DatabaseService {
    constructor() {
        this.connectionConfig = {
            user: "postgres",
            database: "TP4",
            password: "root",
            port: 5432,
            host: "127.0.0.1",
            keepAlive: true
        };
        this.pool = new pg.Pool(this.connectionConfig);
    }
    // ======= DEBUG =======
    getAllFromTable(tableName) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = yield this.pool.connect();
            const res = yield client.query(`SELECT * FROM ${tableName};`);
            client.release();
            return res;
        });
    }
    filterPlanrepas() {
        return __awaiter(this, void 0, void 0, function* () {
            const client = yield this.pool.connect();
            const queryText = "SELECT * FROM Planrepas;";
            const res = yield client.query(queryText);
            client.release();
            return res;
        });
    }
    filterFournisseur() {
        return __awaiter(this, void 0, void 0, function* () {
            const client = yield this.pool.connect();
            const queryText = "SELECT * FROM Fournisseur";
            const res = yield client.query(queryText);
            client.release();
            return res;
        });
    }
    updatePlanrepas(planrepas) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = yield this.pool.connect();
            let toUpdateValues = [];
            if (planrepas.numerofournisseur.length > 0)
                toUpdateValues.push(`numerofournisseur = '${planrepas.numerofournisseur}'`);
            if (planrepas.categorie.length > 0)
                toUpdateValues.push(`categorie = '${planrepas.categorie}'`);
            if (planrepas.frequence > 0)
                toUpdateValues.push(`frequence = ${planrepas.frequence}`);
            if (planrepas.nbrpersonnes > 0)
                toUpdateValues.push(`nbrpersonnes = ${planrepas.nbrpersonnes}`);
            if (planrepas.nbcalories > 0)
                toUpdateValues.push(`nbcalories = ${planrepas.nbcalories}`);
            if (planrepas.prix > 0)
                toUpdateValues.push(`prix = ${planrepas.prix}`);
            if (!planrepas.numeroplan ||
                planrepas.numerofournisseur.length === 0 ||
                planrepas.categorie.length === 0 ||
                planrepas.frequence <= 0 ||
                planrepas.nbrpersonnes <= 0 ||
                planrepas.nbcalories <= 0 ||
                planrepas.prix <= 0 ||
                toUpdateValues.length === 0)
                throw new Error("Invalid planrepas update query");
            const query = `UPDATE Planrepas SET ${toUpdateValues.join(", ")} WHERE numeroplan = '${planrepas.numeroplan}';`;
            const res = yield client.query(query);
            client.release();
            return res;
        });
    }
    createPlanrepas(planrepas) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = yield this.pool.connect();
            if (!planrepas.numeroplan ||
                !planrepas.numerofournisseur.length ||
                !planrepas.categorie.length ||
                !planrepas.frequence ||
                !planrepas.nbrpersonnes ||
                !planrepas.nbcalories ||
                !planrepas.prix)
                throw new Error("Invalid create planrepas values");
            const values = [planrepas.numeroplan, planrepas.numerofournisseur, planrepas.categorie, planrepas.frequence, planrepas.nbrpersonnes, planrepas.nbcalories, planrepas.prix];
            const queryText = `INSERT INTO Planrepas VALUES($1, $2, $3, $4, $5, $6, $7);`;
            const res = yield client.query(queryText, values);
            client.release();
            return res;
        });
    }
    deletePlanrepas(numeroplan) {
        return __awaiter(this, void 0, void 0, function* () {
            const client = yield this.pool.connect();
            const query = `DELETE FROM Planrepas WHERE numeroplan = '${numeroplan}';`;
            const res = yield client.query(query);
            client.release();
            return res;
        });
    }
};
DatabaseService = __decorate([
    (0, inversify_1.injectable)()
], DatabaseService);
exports.DatabaseService = DatabaseService;
//# sourceMappingURL=database.service.js.map