"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseController = void 0;
const express_1 = require("express");
const inversify_1 = require("inversify");
const database_service_1 = require("../services/database.service");
const types_1 = require("../types");
let DatabaseController = class DatabaseController {
    constructor(databaseService) {
        this.databaseService = databaseService;
    }
    get router() {
        const router = (0, express_1.Router)();
        router.get("/Planrepas", (req, res, _) => {
            this.databaseService
                .filterPlanrepas()
                .then((result) => {
                const planrepas = result.rows.map((planrepas) => ({
                    numeroplan: planrepas.numeroplan,
                    numerofournisseur: planrepas.numerofournisseur,
                    categorie: planrepas.categorie,
                    frequence: planrepas.frequence,
                    nbrpersonnes: planrepas.nbrpersonnes,
                    nbcalories: planrepas.nbcalories,
                    prix: planrepas.prix,
                }));
                res.json(planrepas);
            })
                .catch((e) => {
                console.error(e.stack);
            });
        });
        router.put("/Planrepas/update", (req, res, _) => {
            const planrepas = {
                numeroplan: req.body.numeroplan,
                numerofournisseur: req.body.numerofournisseur ? req.body.numerofournisseur : "",
                categorie: req.body.categorie ? req.body.categorie : "",
                frequence: req.body.frequence ? parseInt(req.body.frequence, 10) : -1,
                nbrpersonnes: req.body.nbrpersonnes ? parseInt(req.body.nbrpersonnes) : -1,
                nbcalories: req.body.nbcalories ? parseInt(req.body.nbcalories) : -1,
                prix: req.body.prix ? parseInt(req.body.prix) : -1,
            };
            this.databaseService
                .updatePlanrepas(planrepas)
                .then((result) => {
                res.json(result.rowCount);
            })
                .catch((e) => {
                console.error(e.stack);
            });
        });
        router.post("/Planrepas/insert", (req, res, _) => {
            const planrepas = {
                numeroplan: req.body.numeroplan,
                numerofournisseur: req.body.numerofournisseur,
                categorie: req.body.categorie,
                frequence: parseInt(req.body.frequence, 10),
                nbrpersonnes: parseInt(req.body.nbrpersonnes),
                nbcalories: parseInt(req.body.nbcalories),
                prix: parseInt(req.body.prix),
            };
            this.databaseService
                .createPlanrepas(planrepas)
                .then((result) => {
                res.json(result.rowCount);
            })
                .catch((e) => {
                console.error(e.stack);
                res.json(-1);
            });
        });
        router.delete("/Planrepas/:numeroplan", (req, res, _) => __awaiter(this, void 0, void 0, function* () {
            yield this.databaseService.deletePlanrepas(req.params.numeroplan)
                .then((result) => {
                res.json(result.rowCount);
            })
                .catch((e) => {
                console.error(e.stack);
                res.json(-1);
            });
        }));
        router.get("/Fournisseur", (req, res, _) => {
            this.databaseService
                .filterFournisseur()
                .then((result) => {
                const fournisseur = result.rows.map((fournisseur) => ({
                    numerofournisseur: fournisseur.numerofournisseur,
                    nomfournisseur: fournisseur.nomfournisseur,
                    adressefournisseur: fournisseur.adressefournisseur,
                }));
                res.json(fournisseur);
            })
                .catch((e) => {
                console.error(e.stack);
            });
        });
        return router;
    }
};
DatabaseController = __decorate([
    (0, inversify_1.injectable)(),
    __param(0, (0, inversify_1.inject)(types_1.default.DatabaseService)),
    __metadata("design:paramtypes", [database_service_1.DatabaseService])
], DatabaseController);
exports.DatabaseController = DatabaseController;
//# sourceMappingURL=database.controller.js.map