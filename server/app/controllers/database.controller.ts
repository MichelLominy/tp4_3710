import { NextFunction, Request, Response, Router } from "express";
import { inject, injectable } from "inversify";
import { DatabaseService } from "../services/database.service";
import Types from "../types";
import {Planrepas} from "../../../common/tables/Planrepas"
import {Fournisseur} from "../../../common/tables/Fournisseur"

import * as pg from "pg";
@injectable()
export class DatabaseController {
  
  public constructor(
    @inject(Types.DatabaseService) private readonly databaseService: DatabaseService
  ) {}

  public get router(): Router {
    const router: Router = Router();
    router.get("/Planrepas", (req: Request, res: Response, _: NextFunction) => {
      this.databaseService
        .filterPlanrepas()
        .then((result: pg.QueryResult) => {
          const planrepas: Planrepas[] = result.rows.map((planrepas: Planrepas) => ({
            numeroplan: planrepas.numeroplan,
            numerofournisseur: planrepas.numerofournisseur,
            categorie: planrepas.categorie,
            frequence: planrepas.frequence,
            nbrpersonnes: planrepas.nbrpersonnes,
            nbcalories: planrepas.nbcalories,
            prix: planrepas.prix,
          }));
          res.json(planrepas);
        })
        .catch((e: Error) => {
          console.error(e.stack);
        });
    });

    router.put(
      "/Planrepas/update",
      (req: Request, res: Response, _: NextFunction) => {
        const planrepas: Planrepas = {
          numeroplan : req.body.numeroplan,
          numerofournisseur : req.body.numerofournisseur ? req.body.numerofournisseur : "",
          categorie : req.body.categorie ? req.body.categorie : "",
          frequence : req.body.frequence ? parseInt(req.body.frequence, 10) : -1,
          nbrpersonnes : req.body.nbrpersonnes ? parseInt(req.body.nbrpersonnes) : -1,
          nbcalories : req.body.nbcalories ? parseInt(req.body.nbcalories) : -1,
          prix : req.body.prix ? parseInt(req.body.prix) : -1,
        };

        this.databaseService
          .updatePlanrepas(planrepas)
          .then((result: pg.QueryResult) => {
            res.json(result.rowCount);
          })
          .catch((e: Error) => {
            console.error(e.stack);
          });
      }
    );
    
    router.post(
      "/Planrepas/insert",
      (req: Request, res: Response, _: NextFunction) => {
        const planrepas: Planrepas = {
          numeroplan : req.body.numeroplan,
          numerofournisseur : req.body.numerofournisseur,
          categorie : req.body.categorie,
          frequence : parseInt(req.body.frequence, 10),
          nbrpersonnes : parseInt(req.body.nbrpersonnes),
          nbcalories : parseInt(req.body.nbcalories),
          prix : parseInt(req.body.prix),
        };

        this.databaseService
          .createPlanrepas(planrepas)
          .then((result: pg.QueryResult) => {
            res.json(result.rowCount);
          })
          .catch((e: Error) => {
            console.error(e.stack);
            res.json(-1);
          });
      }
    );

    router.delete("/Planrepas/:numeroplan",
    async (req:Request, res: Response, _: NextFunction) => {
      await this.databaseService.deletePlanrepas(req.params.numeroplan)
      .then((result: pg.QueryResult) => {
        res.json(result.rowCount);
      })
      .catch((e: Error) => {
        console.error(e.stack);
        res.json(-1);
      });
    });

    router.get("/Fournisseur", (req: Request, res: Response, _: NextFunction) => {
      this.databaseService
        .filterFournisseur()
        .then((result: pg.QueryResult) => {
          const fournisseur: Fournisseur[] = result.rows.map((fournisseur: Fournisseur) => ({
            numerofournisseur: fournisseur.numerofournisseur,
            nomfournisseur : fournisseur.nomfournisseur,
            adressefournisseur : fournisseur.adressefournisseur,
          }));
          res.json(fournisseur);
        })
        .catch((e: Error) => {
          console.error(e.stack);
        });
    });

    return router;
  }
}
