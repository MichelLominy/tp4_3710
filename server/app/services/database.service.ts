import { injectable } from "inversify";
import * as pg from "pg";
import "reflect-metadata";
import { Planrepas } from "../../../common/tables/Planrepas";

@injectable()
export class DatabaseService {
  public connectionConfig: pg.ConnectionConfig = {
    user: "postgres",
    database: "TP4",
    password: "root",
    port: 5432,          // Attention ! Peut aussi être 5433 pour certains utilisateurs
    host: "127.0.0.1",
    keepAlive: true
  };

  public pool: pg.Pool = new pg.Pool(this.connectionConfig);

  // ======= DEBUG =======
  public async getAllFromTable(tableName: string): Promise<pg.QueryResult> {
    const client = await this.pool.connect();
    const res = await client.query(`SELECT * FROM ${tableName};`);
    client.release();
    return res;
  }
  

  public async filterPlanrepas(): Promise<pg.QueryResult>  {
    const client = await this.pool.connect();

    const queryText = "SELECT * FROM Planrepas;";

    const res = await client.query(queryText);
    client.release();
    return res;
  }

  public async filterFournisseur(): Promise<pg.QueryResult>  {
    const client = await this.pool.connect();
    const queryText = "SELECT * FROM Fournisseur";
    const res = await client.query(queryText);
    client.release();
    return res;
  }

  public async updatePlanrepas(planrepas: Planrepas): Promise<pg.QueryResult> {
    const client = await this.pool.connect();

    let toUpdateValues = [];

    if (planrepas.numerofournisseur.length > 0) toUpdateValues.push(`numerofournisseur = '${planrepas.numerofournisseur}'`);
    if (planrepas.categorie.length > 0) toUpdateValues.push(`categorie = '${planrepas.categorie}'`);
    if (planrepas.frequence > 0) toUpdateValues.push(`frequence = ${planrepas.frequence}`);
    if (planrepas.nbrpersonnes > 0) toUpdateValues.push(`nbrpersonnes = ${planrepas.nbrpersonnes}`);
    if (planrepas.nbcalories > 0) toUpdateValues.push(`nbcalories = ${planrepas.nbcalories}`);
    if (planrepas.prix > 0) toUpdateValues.push(`prix = ${planrepas.prix}`);

    if (
      !planrepas.numeroplan ||
      planrepas.numerofournisseur.length === 0 ||
      planrepas.categorie.length === 0 ||
      planrepas.frequence <= 0 ||
      planrepas.nbrpersonnes <= 0 ||
      planrepas.nbcalories <= 0 ||
      planrepas.prix <= 0 ||
      toUpdateValues.length === 0
    )
      throw new Error("Invalid planrepas update query");

    const query = `UPDATE Planrepas SET ${toUpdateValues.join(
      ", "
    )} WHERE numeroplan = '${planrepas.numeroplan}';`;
    const res = await client.query(query);
    client.release();
    return res;
  }

  public async createPlanrepas(planrepas: Planrepas): Promise<pg.QueryResult> {
    const client = await this.pool.connect();

    if (!planrepas.numeroplan ||
      !planrepas.numerofournisseur.length ||
      !planrepas.categorie.length ||
      !planrepas.frequence ||
      !planrepas.nbrpersonnes ||
      !planrepas.nbcalories ||
      !planrepas.prix
      )
      throw new Error("Invalid create planrepas values");

    const values: (string|number)[] = [planrepas.numeroplan, planrepas.numerofournisseur, planrepas.categorie, planrepas.frequence, planrepas.nbrpersonnes, planrepas.nbcalories, planrepas.prix];
    const queryText: string = `INSERT INTO Planrepas VALUES($1, $2, $3, $4, $5, $6, $7);`;

    const res = await client.query(queryText, values);
    client.release();
    return res;
  }

  public async deletePlanrepas(numeroplan: string){
    const client = await this.pool.connect();
    const query = `DELETE FROM Planrepas WHERE numeroplan = '${numeroplan}';`;
    const res = await client.query(query);
    client.release();
    return res;
  }
}
