/*1*/
SELECT DISTINCT Client.numeroclient, Client.nomclient 
FROM Client, Abonner, Planrepas 
WHERE Client.numeroclient = Abonner.numeroclient 
AND Planrepas.numeroplan = Abonner.numeroplan 
AND Planrepas.prix BETWEEN 20 and 40;

/*2*/
SELECT Planrepas.numeroplan 
FROM Planrepas, Fournisseur 
WHERE Planrepas.numerofournisseur = Fournisseur. numerofournisseur 
AND (Fournisseur.nomfournisseur <> 'QC Transport' OR Fournisseur.nomfournisseur IS NULL);

/*3*/
SELECT Famille.numeroplan
FROM Famille, Planrepas
WHERE Famille.numeroplan = Planrepas.numeroplan
AND Planrepas.categorie = 'cétogène';

/*4*/
SELECT COUNT(*) 
FROM Fournisseur 
WHERE Fournisseur.nomfournisseur IS NULL;

/*5*/
SELECT DISTINCT Fournisseur.nomfournisseur 
FROM Fournisseur, Planrepas 
WHERE Fournisseur.numerofournisseur = Planrepas.numerofournisseur 
AND Planrepas.prix > (	SELECT MAX(Planrepas.prix) 
FROM Fournisseur, Planrepas 
WHERE Fournisseur.numerofournisseur = Planrepas.numerofournisseur 
AND Fournisseur.nomfournisseur = 'AB Transport');

/*6*/
SELECT Fournisseur.nomfournisseur, Fournisseur.adresseFournisseur, SUM(Planrepas.prix)
FROM Fournisseur, Planrepas
WHERE Fournisseur.numerofournisseur = Planrepas.numerofournisseur
GROUP BY Fournisseur.nomfournisseur, Fournisseur.adresseFournisseur
ORDER BY SUM(Planrepas.prix) desc LIMIT 2;

/*7*/
SELECT count(Kitrepas.numerokitrepas)
FROM Kitrepas, Planrepas
WHERE Planrepas.numeroplan = Kitrepas.numeroplan
AND Planrepas.numeroplan NOT IN(
SELECT Planrepas.numeroplan
FROM Planrepas, Abonner
WHERE Planrepas. numeroplan = Abonner.numeroplan);

/*8*/
SELECT Client.numeroclient, Client.nomclient, Client.prenomclient  
FROM Client, Fournisseur  
WHERE Fournisseur.nomfournisseur= 'Benjamin'  
AND Client.villeclient = Fournisseur.adresseFournisseur 
AND LEFT(Client.prenomclient, 1) NOT IN('a','e','i', 'o','u','y', 'A', 'E', 'I', 'O', 'U', 'Y')  
ORDER BY Client.nomclient ASC;

/*9*/
SELECT Ingredient.paysingredient, count(Ingredient.paysingredient) as nbingredients
FROM Ingredient  
WHERE Ingredient.paysingredient NOT LIKE '%g__'  
GROUP BY Ingredient.paysingredient 
ORDER BY Ingredient.paysingredient DESC;

/*10*/
CREATE OR REPLACE VIEW V_fournisseur(V_Categorie, V_tot, V_adresse) AS
SELECT Planrepas.categorie, sum(Planrepas.prix), Fournisseur.adresseFournisseur  
FROM Fournisseur, Planrepas  
WHERE Fournisseur.numerofournisseur = Planrepas.numerofournisseur  
AND Planrepas.categorie LIKE '%e%'  
AND Planrepas.categorie LIKE '%o__' 
GROUP BY Planrepas.categorie, Fournisseur.adresseFournisseur 
HAVING SUM(Planrepas.prix) > 12500 
ORDER BY Planrepas.categorie ASC, sum(Planrepas.prix) DESC; 

SELECT * from V_fournisseur;
