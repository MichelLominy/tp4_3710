CREATE TABLE IF NOT EXISTS Client (
numeroclient CHAR(4),
nomclient VARCHAR(150),
prenomclient VARCHAR(150),
adressecourrielclient VARCHAR(150),
rueclient VARCHAR(150),
villeclient VARCHAR(150),
codepostalclient CHAR(6),
PRIMARY KEY(numeroclient)
);

CREATE TABLE IF NOT EXISTS Fournisseur(
numerofournisseur CHAR(4),
nomfournisseur VARCHAR(150),
adresseFournisseur VARCHAR(150),
PRIMARY KEY(numerofournisseur)
);

CREATE TABLE IF NOT EXISTS Telephone(
numerodetelephone CHAR(10),
numeroclient CHAR(4),
PRIMARY KEY (numerodetelephone, numeroclient),
FOREIGN KEY (numeroclient) REFERENCES Client (numeroclient) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Planrepas (
numeroplan CHAR(4),
numerofournisseur CHAR(4) NOT NULL,
categorie VARCHAR(15),
frequence INTEGER,
nbrpersonnes INTEGER,
nbcalories INTEGER,
prix INTEGER,
PRIMARY KEY (numeroplan),
FOREIGN KEY (numerofournisseur) REFERENCES Fournisseur(numerofournisseur) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Famille(
numeroplan CHAR(4),
PRIMARY KEY (numeroplan),
FOREIGN KEY (numeroplan) REFERENCES Planrepas(numeroplan) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Vegetarien (
numeroplan CHAR(4),
typederepas VARCHAR(50),
PRIMARY KEY (numeroplan),
FOREIGN KEY (numeroplan) REFERENCES Planrepas(numeroplan) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Pescetarien (
numeroplan CHAR(4),
typepoisson VARCHAR(50),
PRIMARY KEY (numeroplan),
FOREIGN KEY (numeroplan) REFERENCES Planrepas(numeroplan) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Rapide (
numeroplan CHAR(4),
tempsdepreparation INTEGER,
PRIMARY KEY (numeroplan),
FOREIGN KEY (numeroplan) REFERENCES Famille(numeroplan) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Facile (
numeroplan CHAR(4),
nbingredients INTEGER,
PRIMARY KEY (numeroplan),
FOREIGN KEY (numeroplan) REFERENCES Famille (numeroplan) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Kitrepas (
numerokitrepas CHAR(4),
description VARCHAR(300),
numeroplan CHAR(4) NOT NULL,
PRIMARY KEY (numerokitrepas),
FOREIGN KEY (numeroplan) REFERENCES Planrepas(numeroplan) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Image(
numeroimage CHAR(4) ,
donnees VARCHAR(40000), 
numerokitrepas CHAR(4) NOT NULL,
PRIMARY KEY (numeroimage),
FOREIGN KEY (numerokitrepas) REFERENCES Kitrepas(numerokitrepas) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Etape(
numerokitrepas CHAR(4),
descriptionetape VARCHAR, 
dureeetape INTEGER, 
numerokitrepasêtrecomposede INTEGER NOT NULL,
PRIMARY KEY (numerokitrepas),
FOREIGN KEY (numerokitrepas) REFERENCES Kitrepas(numerokitrepas) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Ingredient(
numeroingredient CHAR(4), 
nomingredient VARCHAR(150), 
paysingredient VARCHAR(150),
PRIMARY KEY (numeroingredient)
);

CREATE TABLE IF NOT EXISTS Abonner(
numeroclient CHAR(4),
numeroplan CHAR(4),
duree INTEGER NOT NULL,
PRIMARY KEY (numeroclient, numeroplan),
FOREIGN KEY (numeroclient) REFERENCES Client(numeroclient), 
FOREIGN KEY (numeroplan) REFERENCES Planrepas(numeroplan) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Contenir(
numerokitrepas CHAR(4),
numeroingredient CHAR(4),
PRIMARY KEY (numerokitrepas, numeroingredient),
FOREIGN KEY (numerokitrepas) REFERENCES Kitrepas(numerokitrepas) ON DELETE CASCADE,
FOREIGN KEY (numeroingredient) REFERENCES Ingredient(numeroingredient) ON DELETE CASCADE
);


insert into Client values('c001', 'Keddache', 'Wassim', 'beauGosseWass_pokemon@hotmail.com', 'WassStreet', 'Toronto', 'W4S5WS');
insert into Client values('c002', 'Santanna', 'Abdel', 'AbdelTooHot_Kamehameha@hotmail.com', 'SantannaStreet', 'Las Vegas', 'ABDSAN');
insert into Client values('c003', 'Lominy', 'Michel', 'michelGoku3000@gmail.com', 'TheStreet', 'Compton', 'M1C7O1');

insert into Fournisseur values('f001', NULL, 'New york');
insert into Fournisseur values('f002', 'QC Transport', 'Quebec');
insert into Fournisseur values('f003', 'Benjamin', 'Toronto');
insert into Fournisseur values('f004', 'V_fournisseur', 'V_adresse');
insert into Fournisseur values('f005', 'AB Transport', 'Alberta');
insert into Fournisseur values('f006', 'FourniRien', 'NullPart');

insert into Telephone values('5144444444', 'c001');
insert into Telephone values('5145555555', 'c002');
insert into Telephone values('5143333333', 'c003');

insert into Planrepas values('p001', 'f001', 'cétogène', '1', '3', '700', '20');
insert into Planrepas values('p002', 'f002', 'V_catégorie', '1', '3', '250', '30');
insert into Planrepas values('p003', 'f002', 'Carnivore', '1', '3', '1300', '70');
insert into Planrepas values('p004', 'f003', 'Herbivore', '1', '3', '459', '25');
insert into Planrepas values('p005', 'f004', 'Frugivore', '1', '3', '222', '15');
insert into Planrepas values('p006', 'f005', 'Wassivore', '1', '3', '1234', '90');
insert into Planrepas values('p007', 'f004', 'Abdelivore', '1', '3', '940', '2000');

insert into Famille values('p001');
insert into Famille values('p004');
insert into Vegetarien values('p002', 'merdique');
insert into Vegetarien values('p005', 'spectaculaire');
insert into Pescetarien values('p001', 'pourrie');
insert into Pescetarien values('p003', 'quiVieDansLeau');
insert into Rapide values('p001', '2');
insert into Facile values('p004', '3');

insert into Kitrepas values('k001', 'voici une description', 'p001'); 
insert into Kitrepas values('k002', 'voici une description', 'p002'); 
insert into Kitrepas values('k003', 'voici une description', 'p004'); 
insert into Kitrepas values('k004', 'voici une description', 'p005'); 
insert into Kitrepas values('k005', 'voici une description', 'p007'); 
insert into Kitrepas values('k006', 'voici une description', 'p006'); 

insert into Image values('i001', '01010010101011111', 'k002');
insert into Image values('i002', '111000011100110111', 'k002');

insert into Ingredient values('g001', 'patate douce', 'Ouganda');
insert into Ingredient values('g002', 'riz', 'Argentine');
insert into Ingredient values('g003', 'Banane', 'Haiti');
insert into Ingredient values('g004', 'saumon', 'Japon');
insert into Ingredient values('g005', 'pomme', 'Mexique');

insert into Abonner values('c001', 'p002', '4');
insert into Abonner values('c002', 'p004', '3');
insert into Abonner values('c001', 'p003', '76');
insert into Abonner values('c003', 'p006', '43');

insert into Contenir values('k002', 'g002');
insert into Contenir values('k001', 'g005');
insert into Contenir values('k004', 'g002');
insert into Contenir values('k002', 'g003');
insert into Contenir values('k005', 'g001');

