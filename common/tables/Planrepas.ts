export interface Planrepas {
    numeroplan :string;
    numerofournisseur :string;
    categorie :string;
    frequence : number;
    nbrpersonnes : number;
    nbcalories : number;
    prix : number;
}